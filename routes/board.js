const Board = require('../models/board'),
{Post} = require('../models/post'),
Thread = require('../models/thread'),
utils = require('../utils')
module.exports = async function(board, server) {

  const board_short = board.short;

  //get general board info
  server.get(`/${board_short}/`, async (req, res, next)=>{
    console.log('borad')
    console.log(`serving root board data for ${board.short}`)
    res.json(200, Object.assign(
      board.toObject(),
      {
        threads: await board.getThreads()
      }
    ))
  });


  //post new thread
  server.post(`/${board_short}/post`, async (req, res, next) => {
    try {
      let data = {
        post: req.body
      }

      if(req.files && req.files.media){
        data.files = req.files;
      } else {
        throw new Error('Must contain an image for a new thread')
      }

      const thread = await Thread.create({board:board.id});

      const post = await thread.addOpPost(board, data);

      return res.json(200, thread);
    } catch (err) {
      console.log(err)
      return res.json(500, {err: err.message});
    }
    next();

  });

  require('./thread')(board, server)

}
