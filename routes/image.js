const {Post} = require('../models/post')
const Thread = require('../models/thread')
const fs = require('fs')
const path = require('path');
const utils = require('../utils');
module.exports = async function(board, server) {

  server.get(new RegExp(`/media/${board.short}/(\\d+?)\\.(.{2,4})`), async (req, res, next)=>{
    try {

      const thread = await Thread.findByBoardIdAndPostNo({
        board_id:board.id,
        postNo:req.params[0],
      });

      const post = await thread.findPostWithNo(req.params[0]);

      //const post = await Post.findByThreadIdAndPostNo({threadId:thread.id, postNo:req.params[0]});

      if(post.media && post.media.path){
        if(req.params[1] !== post.media.ext){
          throw new Error('File is not correct filetype requested');
        }
        await utils.serveFile(post.media, res);


      } else {
        res.json(404, {err:'That resource does not exist'})
      }

    } catch (err){
      return res.json(500, {err:err.message})
    }
    next();
  });



}
