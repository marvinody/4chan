'use strict';
const Board = require('../models/board')
const Thread = require('../models/thread')
const {Post} = require('../models/post')
const utils = require('../utils')
module.exports = function(board, server) {

  //retrive a thread
  server.get(`/${board.short}/:post_no/`, async (req, res, next) => {
    try {
      const thread = await Thread.findByBoardIdAndOpPostNo({
        board_id:board.id,
        postNo:req.params.post_no
      });
      if (!thread){
        throw new Error('Cannot find a thread with that post number.')
      }
      return res.json(200,
        Object.assign(
          thread.toObject(),
          {idx:await thread.getIdx()}
        )
      );
    } catch (err) {
      return res.json(500, {err:err.message})
    }
    next();

  })

  //post new post in an existing thread
  server.post(`/${board.short}/:post_no/post`, async (req, res, next) => {
    try {
      const thread = await Thread.findByBoardIdAndOpPostNo({
        board_id:board.id,
        postNo:req.params.post_no
      });

      if(thread.is_archived){
        throw new Error('Cannot post to archived thread!')
      }
      console.log(req.body);
      req.body.isOp = false;
      let data = {
        post: req.body
      }

      if(req.files && req.files.media){
        console.log('media');
        data.files = req.files;
      }

      const post = await thread.addPost(board, data);
      res.json(200, post.toObject())

    } catch (err) {
      res.json(500, {err:err.message})
    }

  });


}
