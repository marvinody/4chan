const readChunk = require('read-chunk'),
fileType = require('file-type'),
fs = require('fs'),
{promisify} = require('util'),
fileStats = promisify(fs.stat),
rename = promisify(fs.rename),
mv = promisify(require('mv')),
config = require('./config'),
path = require('path'),
uuid = require('uuid/v4')
var exports = {};

const getFileData = exports.getFileData = async function(media){
  const buffer =  await readChunk(media.path, 0, 4100);
  const fileInfo = fileType(buffer);

  return await Object.assign({},
    fileType(buffer),
    {
      name: media.name,
      size: ((await fileStats(media.path)).size / 1024) | 0
    }

  )
},
constructStringFromObj = exports.constructStringFromObj = function(prefix, keys, s){
  return Object.keys(keys).reduce((acc, k) => acc.replace(`${prefix}${k}`,keys[k]),s)
}
constructFilePath = exports.constructFilePath = function(board, post){
  //make with uuid. 8bits * 16 = 128 bits
  const id = Buffer.alloc(16);
  uuid({},id);
  const filename = `${board.short}/${id.toString('hex')}.${post.media.ext}`;
  fullpath = path.join(config.media.location, filename);
  return fullpath

},

constructURL = exports.constructURL = function(board, post){

  validVars = {
    board: board.short,
    post: post.postNo,
    ext: post.media.ext,
  }
  const naming = 'media/#board/#post.#ext'
  return constructStringFromObj('#', validVars, naming);
},
subset = exports.subset = function(obj, keys_s){
  if(typeof keys_s === 'string'){
    keys_s = keys_s.split(' ');
  }
  return keys_s.reduce((o, k)=>{
    if(k.indexOf('.') > -1){
      const m_ks = k.split('.'),
      l = m_ks.length;
      //basically walk through the dot string
      //and update stuff
      //create new obj when needed and traverse into it
      //and at the end, set stuff equal to each other
      m_ks.reduce((acc, t_k, i) => {
        if (i + 1 < l){
          acc.o[t_k] = {};
          acc.o = acc.o[t_k]
          acc.obj = acc.obj[t_k];
        } else {
          acc.o[t_k] = acc.obj[t_k];
        }
        return acc
      },{//this is so we keep track of both inside
        o:o,
        obj:obj,
      })
    } else {
      //just a simple name, don't do magic
      o[k] = obj[k];
    }


    return o
  }, {});
},
getAbsoluteMediaPath = exports.getAbsoluteMediaPath = function(media_path){
  return path.join(__dirname, media_path);
}
moveFile = exports.moveFile = async function(from, to){
  console.log(`from: ${from}`)
  console.log(`to: ${to}`)
  return await mv(from, to, {mkdirp: true})
},
serveFile = exports.serveFile = async function(media, res){
  const fullpath = getAbsoluteMediaPath(media.path);
  res.setHeader('Content-Type', media.mime)
  res.setHeader('Content-Disposition', `inline;filename=${media.name}`)
  res.writeHead(200);
  var filestream = fs.createReadStream(fullpath);
  filestream.pipe(res);
},
deletePropertyPath = exports.deletePropertyPath = function (obj, path) {
  //https://stackoverflow.com/questions/40712249
  if (!obj || !path) {
    return;
  }
  if (typeof path === 'string') {
    path = path.split('.');
  }
  for (var i = 0; i < path.length - 1; i++) {
    obj = obj[path[i]];
    if (typeof obj === 'undefined') {
      return;
    }
  }
  delete obj[path.pop()];
};

module.exports = exports;
