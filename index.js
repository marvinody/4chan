'use strict'

/**
* Module Dependencies
*/
const restify = require('restify'),
mongoose = require('mongoose');
/**
* Config
*/
const config = require('./config')

/**
* Initialize Server
*/
const server = restify.createServer({
  name    : config.name,
  version : config.version
})

/**
* Bundled Plugins (http://restify.com/#bundled-plugins)
*/
//server.use(restify.plugins.jsonBodyParser({ mapParams: true }))
server.use(restify.plugins.acceptParser(server.acceptable))
server.use(restify.plugins.queryParser({ mapParams: true }))
server.use(restify.plugins.fullResponse())
server.use(restify.plugins.bodyParser({mapParams:true, multiples: false}))

/**
* Lift Server, Connect to DB & Require Route File
*/
server.listen(config.port, () => {

  const opts = {
    promiseLibrary: global.Promise,
    auto_reconnect: true,
    reconnectTries: Number.MAX_VALUE,
    reconnectInterval: 1000,
    autoIndex: true,

  }


  const db = mongoose.connection

  db.on('error', (err) => {
    if (err.message.code === 'ETIMEDOUT') {
      console.log(err)
      mongoose.connect(config.db.uri, opts)
    }
  })

  db.once('open', async () => {




    console.log(`Server is listening on port ${config.port}`)


    const Board = require('./models/board');

    let boards = config.boards

    try {
      boards = await Promise.all(
        boards.map( async (boardData) => {
          console.log(`Updating: ${boardData.short}`)
          //new and upsert and defaults so we create on first run
          const board = await Board.findOneAndUpdate(
            {short:boardData.short},boardData,
            {upsert: true, setDefaultsOnInsert: true, new: true}
          ).exec()
          //and then create routes for each board
          //I create route per board instead of a global catch so I
          //can 'cache' the board object and not really require another call
          await require('./routes/board')(board, server)
          await require('./routes/image')(board, server)
          return board;
        }));
        //just log out the boards so on the same page
        console.log(boards)
      } catch (err) {
        console.log(err);
      }

//serve some basic stuff on root page
server.get('/', function(req, res, next){
  res.send(200, boards.map(b => b.toObject()))
});


//catches everything else and just 404s
server.get(/.+/, (req, res, next)=>{
  console.log('last route')
  console.log(req.url);
  res.json(404, {err:'That page does not exist'})
})


console.log(
  '%s v%s ready to accept connections on port %s in %s environment.',
  server.name,
  config.version,
  config.port,
  config.env
)
})


mongoose.connect(config.db.uri, opts)



})


process.on('unhandledRejection', (reason, p) => {
  console.log('Unhandled Rejection at: Promise', p, 'reason:', reason);
  // application specific logging, throwing an error, or other logic here
});
