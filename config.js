'use strict';

module.exports = {
    name: 'node-4chan',
    version: '0.0.1',
    env: process.env.NODE_ENV || 'development',
    port: process.env.PORT || 3000,
    db: {
        uri: 'mongodb://127.0.0.1:27017/4chan',
    },
    boards: [
      {
        short: 'cat',
        long: 'Catboat',
        lastPost: 0
      },
      {
        short: 'jp',
        long: 'Japanese Culture & Otaku w/e',
      },
      {
        short:'b',
        long: 'Random',
      }
    ],
    media: {
      location: 'media/',
    }

}
