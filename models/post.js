const mongoose   = require('mongoose'),
timestamps = require('mongoose-timestamp'),
postNo = require('./plugins/postNo');

const utils = require('../utils')

const PostSchema = new mongoose.Schema({
  content: {
    type: String,
    maxlength:500
  },
  media: {
    ext: String,
    mime: String,
    name: String,
    size: Number,
    url: String,
    path: String

  },
  isOp:{
    type: Boolean,
    default: false,
  },
  postNo: {
    type: Number,
    index: true,
  },


}, { collection: 'posts' })


if (!PostSchema.options.toObject) PostSchema.options.toObject = {};
PostSchema.options.toObject.transform = function(doc, ret, options){
  '_id media.path updatedAt'.split(' ').forEach(k => {
    utils.deletePropertyPath(ret, k);
  })
  return ret;
}

PostSchema.static('findByPostNo', function findByPostNo(postNo){
  return this.find({postNo:postNo})
  .exec()
})

PostSchema.static('findByThreadIdAndPostNo', function findByThreadIdAndPostNo(obj){
  return this.findOne({thread: obj.threadId, postNo: obj.postNo}).exec()
});

PostSchema.static('findByThread', function findByThread(thread_id){
  return this.find({thread:thread_id})
  .select('-_id postNo content media createdAt')
  .sort('postNo').exec()
})

PostSchema.pre('validate', function PostValidator(next){
  if (this.content === undefined && this.media === undefined){
    next(new Error('Post must contain either a comment or media'));
  } else {
    next();
  }
});

// So this is partial because we want to check some stuff beforehand
// once we verify file is ok and move on, we'll create the full file
PostSchema.static('createPartialMediaOjbect', async function createPartialMediaOjbect(board, media_upload){
  const fileData = await utils.getFileData(media_upload)
  if( !board.isValidFileForBoard(fileData)){
    throw new Error('Invalid file uploaded.');
  }
  return fileData

})

//PostSchema.plugin(postNo)
PostSchema.plugin(timestamps,{
  createdAt: {
    indexed: true
  }
})



module.exports = {
  'PostSchema': PostSchema,
  'Post': mongoose.model('Post', PostSchema)
}
