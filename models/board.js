const mongoose   = require('mongoose'),
Thread = require('./thread')
const readChunk = require('read-chunk');
const fileType = require('file-type');
const utils = require('../utils');
//defaults
const allowed_mimetypes = [
  'image/jpeg',
  'image/png',
  'image/gif',
]


const BoardSchema = new mongoose.Schema({
  short: {
    type: String,
    trim: true,
    lowercase: true,
    index: true,
    unique: true,
    required: 'Board must have a shortcode',
  },
  long: {
    type: String,
    trim: true,
    required: 'Board must have a long name',
  },
  totalThreadCount: {
    type: Number,
    default: 100,
  },
  textBumpLimit: {
    type:Number,
    default:250,
  },
  imageBumpLimit:{
    type:Number,
    default:150,
  },
  allowedMimeTypes:{
    type:[String],
    default: allowed_mimetypes
  },
  maxFileSize:{
    type: Number,
    default: 5*1024, //5 megs
  },
  postNo: {
    type: Number,
    default: 0 //will be +1 for the first post
  }
}, { collection: 'boards' })

BoardSchema.method('incAndGetPostNo', async function incAndGetPostNo(){
  this.postNo += 1
  next = this.postNo
  await this.save()
  return next
})

if (!BoardSchema.options.toObject) BoardSchema.options.toObject = {};
BoardSchema.options.toObject.transform = function(doc, ret, options){
  '_id __v postNo'.split(' ').forEach(k => {
    utils.deletePropertyPath(ret, k);
  })
  return ret;
}

BoardSchema.method('isValidFileForBoard',function isValidFileForBoard(media){
  if (!this.allowedMimeTypes.includes(media.mime)){
    throw new Error('File is not of an allowed type for this board');
  }
  if ( media.size > this.maxFileSize){
    throw new Error('File is too large for this board');
  }
  return true;
})

BoardSchema.method('getThreads', async function getThreads(){
  return await Thread.findByBoard(this);
})

module.exports = exports = mongoose.model('Board', BoardSchema)
