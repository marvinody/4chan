const mongoose   = require('mongoose'),
{Post,PostSchema} = require('./post'),
timestamps = require('mongoose-timestamp')
const utils = require('../utils');

const ThreadSchema = new mongoose.Schema({
  board: {
    type: mongoose.Schema.Types.ObjectId, ref: 'Board',
    required: true,
  },
  is_archived: {
    type: Boolean,
    default: false,
  },
  is_hidden: {
    type: Boolean,
    default: false,
  },
  is_deleted: {
    type: Boolean,
    default: false,
  },
  postNo: {
    type: Number,
    index: true
  },
  posts: {
    type: [PostSchema],
  }

}, { collection: 'threads' })

ThreadSchema.plugin(timestamps,{
  updatedAt: {
    indexed: true
  }
})
//TODO add hidden/deleted checks on fetch
//TODO start simple vue client


if (!ThreadSchema.options.toObject) ThreadSchema.options.toObject = {};
ThreadSchema.options.toObject.transform = function(doc, ret, options){
  '_id __v is_hidden is_deleted board'.split(' ').forEach(k => {
    utils.deletePropertyPath(ret, k);
  })
  return ret;
}

ThreadSchema.method('findPostWithNo', async function findPostWithNo(postNo){
  //Assume that a post exists with the postno in this thread
  return this.posts.filter(p => p.postNo == postNo)[0];
})

ThreadSchema.method('getIdx', async function getIdx(){
  const idx = await this.model('Thread').find({
    'is_hidden':false,
    'is_deleted':false,
    'updatedAt': { $gt: this.updatedAt},
  }).count().exec()
  return idx;
})

ThreadSchema.static('findByBoard', async function(board){
  threads = await this.find({
    'board':board.id,
    'is_hidden':false,
    'is_deleted':false
  }).sort('-updatedAt')
  .limit(board.totalThreadCount)
  .exec();
  return threads.map((t,i) => {
    return Object.assign(t.toObject(), {idx:i})
  })
})


ThreadSchema.static('findByBoardIdAndOpPostNo', async function findByBoardIdAndPostNo(obj){
  return await this.findOne({
    'posts':{$elemMatch:{
      'postNo': obj.postNo,
      'isOp':true
    }},
    'board': obj.board_id,
    'is_hidden':false,
    'is_deleted':false,
  }).exec();

})

ThreadSchema.static('findByBoardIdAndPostNo', async function findByBoardIdAndPostNo(obj){
  return await this.findOne({
    'posts':{$elemMatch:{
      'postNo': obj.postNo,
    }},
    'board': obj.board_id,
    'is_hidden':false,
    'is_deleted':false,
  }).exec();

})


ThreadSchema.method('addOpPost', async function addOpPost(board,data){
  data.post.isOp = true;
  const post = await this.addPost(board, data);
  this.postNo = post.postNo;
  await this.save();
  return post;
})

ThreadSchema.method('addPost', async function addPost(board, data){
  //data should have a post obj that contains text content, isOp, and media object if needed
  //and a file object that contains tmp file name of media
  if(!data.post.content && !(data.files && data.files.media)) {
    throw new Error('Post must contain either text or some valid media')
  }
  //if file upload, create partial
  if(data.files && data.files.media){
    data.post.media = await Post.createPartialMediaOjbect(board, data.files.media)
  }
  //grab postNo and then create and get
  data.post.postNo = await board.incAndGetPostNo();
  await this.posts.push(data.post);
  const post = this.posts[this.posts.length-1]

  //and then now that we have the postNo, can get filepath
  //use some hashing algo at some point later maybe
  if(data.files && data.files.media){
    post.media.path = utils.constructFilePath(board, post);
    post.media.url = utils.constructURL(board, post);
    await utils.moveFile(data.files.media.path, post.media.path);
  }

  await this.save();
  return post;

})


module.exports = exports = mongoose.model('Thread', ThreadSchema)
